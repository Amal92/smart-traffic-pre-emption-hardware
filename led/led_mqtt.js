/**
 * Created by amal on 14/01/16.
 */
"use strict";

var Cylon = require("cylon");

Cylon.robot({
    connections: {
        mqtt: {adaptor: "mqtt", host: "tcp://test.mosquitto.org:1883"},
        firmata: {adaptor: "firmata", port: "/dev/cu.usbmodem1421"}
    },

    devices: {
        toggle: {driver: "mqtt", topic: "lamp/light/lat/lon", connection: "mqtt"},
        green_led: {driver: 'led', pin: 12, connection: "firmata"},
        red_led: {driver: 'led', pin: 13, connection: "firmata"}
    },

    work: function (my) {
        my.connections.mqtt.subscribe('vivek/amal/iot/throwdistanceS');
        //  my.connections.mqtt.subscribe('brightness');

        my.connections.mqtt.on('message', function (topic, data) {
            console.log(topic + ": " + data);

            if (topic == "vivek/amal/iot/throwdistanceS") {
                if (data == "on") {
                    my.red_led.turnOff();
                    my.green_led.turnOn();
                }
                if (data == "off") {
                    my.green_led.turnOff();
                    my.red_led.turnOn();
                }
            }

        });

        my.red_led.turnOn();

        var geolocate = require('geolocate');
        geolocate(function(latLong){
            var latitude = latLong[0];
            var longitude = latLong[1];
            var obj = {};
            obj.latitude = latitude;
            obj.longitude = longitude;
            my.toggle.publish(JSON.stringify(obj));
        });


        /*
         my.toggle.on("message", function(data) {
         console.log("Message on 'toggle': " + data);
         my.led.toggle();
         });

         every((1).second(), function() {
         console.log("Toggling LED.");
         my.toggle.publish("Hi vivek");
         });
         */
    }
}).start();
